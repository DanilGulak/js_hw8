let input = document.getElementById('inputText');


input.onfocus = () => input.style.outline = '2px solid green'
let div = document.createElement('div');

div.style.marginBottom = '10px'
let btn = document.createElement('button');

btn.style.marginLeft = '4px'
let span = document.createElement('span');

let validate = document.createElement('p');

validate.textContent = 'Please enter correct price';
validate.style.color = 'red'



input.onchange = () => {
    input.onblur = () => {
        if (!( +input.value < 0 ))
            input.style.borderColor = '';
    }
    if (+input.value >= 0) {
        validate.remove();

        span.textContent = `Текущая цена: ${input.value}`;

        div.appendChild(span);
        btn.textContent = 'X';

        btn.onclick = () => {
            btn.parentElement.remove();
            input.value = ' ';
        }

        div.appendChild(btn);
        document.body.insertBefore(div, document.body.children[0]);
        input.style.outline = '2px solid green';
    } else {
        input.style.outline = '2px solid red';
        document.body.appendChild(validate);
        if (btn.parentElement)
            btn.parentElement.remove();
    }
};
